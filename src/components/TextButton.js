import React, { Component } from 'react';
import '../assts/styles/TextButton.css';

class TextButton extends Component 
{

    render = () =>
    {
        return (
            <div className="textButton" onClick= { this.props.onClick } >
                {this.props.text}
            </div>
        );
    }
}

export default TextButton;