import React, { Component } from 'react';
import '../assts/styles/NavBox.css';
import arrowUpImage from '../assts/icons/arrow-up.png';
import arrowDownImage from '../assts/icons/arrow-down.png';

class NavBox extends Component 
{        
    render = () =>
    {
        return(
            <div className="outerContainer">
                <div className="up" onClick={this.props.onUpClick}>
                    <img className="arrowUpImage" src={arrowUpImage}  alt="up" />
                </div>
                <div className="down" onClick={this.props.onDownClick}>
                    <img className="arrowDownImage" src={arrowDownImage} alt="down" />
                </div>
            </div>
        );
    }        
}

export default NavBox;