import React, { Component } from 'react';
import '../assts/styles/TodoCheckbox.css';

class TodoCheckbox extends Component 
{
    render = () =>
    {
        return (        
        <label className="checkboxContainer">   
            <input type="checkbox" className="todo-checkbox"/>            
            <span className="checkmark"></span>
        </label>
        );                
    }
}

export default TodoCheckbox;