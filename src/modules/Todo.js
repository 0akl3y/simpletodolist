
class Todo 
{
  constructor(title, inEditMode)
  {
    this.title = title;
    this.inEditMode = inEditMode;    
  }
}

export default Todo;