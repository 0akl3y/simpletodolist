import Todo from './Todo';


//constants
const ADD_TODO = 'ADD_TODO';
const DELETE_TODO = 'DELETE_TODO';
const MOVE_TODO_UP = 'MOVE_TODO_UP';
const MOVE_TODO_DOWN = 'MOVE_TODO_DOWN';
const TOGGLE_TODO = 'TOGGLE_TODO';
const SET_TODO_TITLE = 'SET_TODO_TITLE';

//action creators
export function removeRowAt(index)
{
    return {
        type: DELETE_TODO, 
        index
    }
}

export function moveRowUp(index)
{
    return {
        type: MOVE_TODO_UP, 
        index
    }
}

export function moveRowDown(index)
{        
    return {
        type: MOVE_TODO_DOWN, 
        index
    }
} 

export function addTodo(title, inEditMode)
{
    return {
        type: ADD_TODO, 
        title, 
        inEditMode
    }
}

export function toggleEditModeAt(index) 
{
    return {
        type: TOGGLE_TODO, 
        index
    }        
}

export function setRowTitleAt (index, title)
{        
    return {
        type: SET_TODO_TITLE, 
        index, 
        title
    }        
}
//reducer
export const reducer = (state = {todos:[new Todo('Hallo Welt', false)]}, action) =>
{    
    let todosCopy = [...state.todos];
    switch (action.type)
    {
        case ADD_TODO:
            let newTodo = new Todo(action.title, action.inEditMode);
            return {todos:todosCopy.concat([newTodo])};
        case TOGGLE_TODO:            
            todosCopy[action.index].inEditMode = !todosCopy[action.index].inEditMode;
            return {todos: todosCopy};            
        case MOVE_TODO_UP:
            if(action.index - 1 < 0)
            {
                return state;
            }
            return exchangeRowsAt(todosCopy, action.index, action.index-1);
        case MOVE_TODO_DOWN:
            if(action.index + 1 > (state.todos.length - 1) )
            {
                return state;
            }
            return exchangeRowsAt(todosCopy, action.index, action.index + 1);
        case DELETE_TODO:            
            todosCopy.splice(action.index, 1)
            return {todos:todosCopy};
        case SET_TODO_TITLE:
            todosCopy[action.index].title = action.title;        
            return {todos:todosCopy};
        default:
            return state;
    }
}

//helpers

const exchangeRowsAt = (state, indexA, indexB) =>
{            
    let temp = state[indexA];
    state[indexA] = state[indexB];
    state[indexB] = temp;
    return {todos:state};
}