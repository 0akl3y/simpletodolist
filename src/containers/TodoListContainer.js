
import { connect } from 'react-redux'
import TodoTable from '../components/TodoTable'
import { setRowTitleAt, toggleEditModeAt, moveRowDown, moveRowUp, removeRowAt, addTodo} from '../modules/TodoHandling';

const mapStateToProps = (state) => 
{
    return {todos: state.todos}
}

const mapDispatchToProps = (dispatch) =>
{
    return {
        onAddTodoClick: (title, inEditMode) => {dispatch(addTodo(title, inEditMode))},    
        onTodoTitleChange: (index, value) => {dispatch(setRowTitleAt(index, value))},
        onRemoveRowClick: (index) => {dispatch(removeRowAt(index))},
        onToggleEditModeClick: (index) => {dispatch(toggleEditModeAt(index))},
        onMoveRowDownClick: (index) => { dispatch(moveRowDown(index)) },
        onMoveRowUpClick: (index) => { dispatch(moveRowUp(index)) }
    }
}

const TodoListContainer = connect(
    mapStateToProps,
    mapDispatchToProps
)(TodoTable)

export default TodoListContainer